## How to run the project

To start working on this codebase, do the following:

1. Generate your set of Flickr API keys here: https://www.flickr.com/services/api/misc.api_keys.html
2. Create a `config/application.yml` file just as the `config/application.sample.yml`and add the API keys there
3. Project setup:
  `$ docker-compose build`
  `$ docker-compose run --rm web rake db:create`
  `$ docker-compose up`
4. To run the app you can do `docker compose up` or if you want to debug with some tool such byebug or pry you have to run `docker-compose up -d web && docker attach flickr-api_web_1`

The web app will be available at http://localhost:3000

## Running tests

Tests can be run with the following command

  `docker-compose run --rm web rails test`

## Heroku app

The app has been deployed to Heroku here: https://flickr-api-demo.herokuapp.com

## Things to improve

Some of the things I would like to improve if I had more time are:

- Make use of pagination: the API search endpoint returns the page and number of pages, that would help to fetch a limited amount of photos and display a pagination component to get more pages
- Display the photo on it's on page with user's data and buttons that would take to either the photo's author or the photo itself.
- Cache the responses: store the responses in cache so the API is not hit everytime.
