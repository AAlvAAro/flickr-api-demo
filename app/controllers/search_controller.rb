# frozen_string_literal: true

class SearchController < ApplicationController
  def index
    query = params[:q]
    page = 1

    Rails.cache.fetch([query, page]) do
      gateway = Flickr::Gateway.new({ text: params[:q] })
      response = gateway.search
      @photos = response.fetch(:photos)
    end
  end
end
