# frozen_string_literal: true

module ApplicationHelper
  def photo_url(photo)
    "https://live.staticflickr.com/#{photo['server']}/#{photo['id']}_#{photo['secret']}_w.jpg"
  end

  def flickr_photo_url(photo)
    "https://www.flickr.com/photos/#{photo['owner']}/#{photo['id']}"
  end
end
