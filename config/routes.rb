Rails.application.routes.draw do
  root to: 'home#index'

  get 'search/index', as: :search
end
