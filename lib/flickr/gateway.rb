# frozen_string_literal: true

module Flickr
  class Gateway
    include HTTParty

    base_uri 'https://flickr.com/services/rest'

    def initialize(request_opts)
      api_key = ENV['FLICKR_API_KEY']
      access_token = ENV['FLICKR_ACCESS_TOKEN']
      @request_opts = request_opts

      @options = {
        query: {
          api_key: api_key,
          access_token: access_token,
          format: 'json',
          nojsoncallback: '1'
        }
      }
    end

    def search
      @options[:query].merge!({ method: 'flickr.photos.search', text: @request_opts[:text] })
      response = self.class.get('/', @options)

      build_response(response.body)
    end

    private

    def build_response(response)
      parsed_response = JSON.parse(response).fetch('photos')

      {
        page: parsed_response['page'],
        pages: parsed_response['pages'],
        photos: parsed_response['photo']
      }
    end
  end
end
