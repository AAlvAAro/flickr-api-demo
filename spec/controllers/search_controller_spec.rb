require 'rails_helper'

RSpec.describe SearchController, type: :controller do
  let(:response) do
    {
      photos: [
        {
          'id' => 'photo-1',
          'server' => 'photo-server',
          'secret' => 'photo-secret',
          'owner' => 'photo-owner'
        },
        {
          'id' => 'photo-2',
          'server' => 'photo-server',
          'secret' => 'photo-secret',
          'owner' => 'photo-owner-2'
        }
      ]
    }
  end

  describe '#GET index' do
    it 'calls the API gateway to get the photos from a some query text' do
      allow_any_instance_of(Flickr::Gateway).to receive(:search) { response }

      get :index
      expect(assigns(:photos)).to eq(response[:photos])
    end
  end
end
