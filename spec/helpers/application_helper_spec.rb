require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  let(:photo) do
    {
      'id' => 'photo-id',
      'server' => 'photo-server',
      'secret' => 'photo-secret',
      'owner' => 'photo-owner'
    }
  end

  describe 'photo_url' do
    it 'generates an image URL to display the photo on the site' do
      expect(photo_url(photo)).to eq('https://live.staticflickr.com/photo-server/photo-id_photo-secret_w.jpg')
    end
  end

  describe 'flickr_photo_url' do
    it 'generates a URL to link to the original photo at Flickr' do
      expect(flickr_photo_url(photo)).to eq('https://www.flickr.com/photos/photo-owner/photo-id')

    end
  end
end
