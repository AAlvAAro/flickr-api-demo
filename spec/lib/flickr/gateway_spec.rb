require 'rails_helper'

module Flickr
  describe Gateway do
    let(:subject) { described_class.new({ text: 'cats' }) }

    describe '#search' do
      it 'retrieves a response with photos from the API endpoint' do
        VCR.use_cassette('search') do
          response = subject.search

          expect(response[:page]).to be_present
          expect(response[:pages]).to be_present
          expect(response[:photos]).to be_present
        end
      end
    end
  end
end

